<?php

	class Phpassword extends Config{

		/*
		
		CRIPTOGRAFIA

		FORMA DA CRIPTOGRAFIA: 

		PREFIXO	 +#+  MD5(SENHA)	+|+ substr(sha1(SENHA.SALT)) 		+.+  substr(sha1(substr(SENHA, 2, 2)))
		DINAMICO +#+    FIXO    	+|+ 		DINAMICO				+.+				FIXO
		
		OBS.:
		
		DINAMICO 	=> não da pra ser comparado pelo computador 
		FIXO 		=> pode ser comparado pelo computador

		*/

		public function passwordSalt($password){

			$objConfig 		= new Config();
			$newPassword1 	= substr(sha1($password.$objConfig->getDados('salt')),10,25);
			$newPassword2 	= substr(sha1(substr($password, 2, 2)),10,25);
			$newPassword  	= $newPassword1.".".$newPassword2;

			return $newPassword;
		}

		public function passwordCrypt($password){

			$objConfig 		= new Config();
			$passwordSalt 	= Phpassword::passwordSalt($password);
			$password 		= md5($password);	
			$password 		= $objConfig->getDados('prefix')."#".$password."|".$passwordSalt;

			return $password;

		}


	}

?>


