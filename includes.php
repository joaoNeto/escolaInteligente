<?php

include "config.php";
include "bibliotecas/phpassword.php";

/*

principais parametros para o construtor

Config 		=> para chamar as configurações do sistema
Phpassword 	=> para chamar a biblioteca de senhas

*/

class Includes{

	private $classe;

	public function __construct($classe){

		$this->classe = $classe;

	}

	public function __call($metodo,$valor){

		$obj = new $this->classe();
		print $obj->$metodo($valor[0]);
		
	}

}




?>
