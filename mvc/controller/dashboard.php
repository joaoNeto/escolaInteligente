<?php

include "validarDados.php";

class dashboard{

        public function adm(){
                
                $objConfig      = new Config();

                $urlEscola      = $objConfig->getDados('pathModel');
                $urlEscola     .= "escola.php";

                $urlPortal      = $objConfig->getDados('pathModel');
                $urlPortal     .= "portal.php";



                include $urlEscola;
                include $urlPortal;

                $objEscola      = new Escola();
                $objPortal      = new Portal();

                /*      SE NAO ESTIVER LOGADO SER REDIRECIONADO   */
                if(empty($_SESSION['id_adm']) || empty($_SESSION['id_escola'])){
                        $url    = $objConfig->getDados('pathBase');
                        header("location: ".$url);                        
                        die();
                }

                $escola         = $objEscola->buscarEscolaPeloId($_SESSION['id_escola']);

                if( empty($escola->id_portal) ){ 

                        $url    = $objConfig->getDados('pathView');
                        $url   .= "cadastroPortal.php";

                        if( !empty($_POST) ){
                                /*      CHAMAR METODO PARA CADASTRAR PORTAL     */
                                $dados = array();

                                $dados['cor_primaria']          = $_POST['cor_primaria'];
                                $dados['cor_secundaria']        = $_POST['cor_secundaria'];
                                $dados['posicao_login']         = $_POST['posicao_login'];

                                if($_FILES['arquivo']["type"] == 'image/png' || $_FILES['arquivo']["type"] == 'image/jpeg' || $_FILES['arquivo']["type"] == 'image/gif' || $_FILES['arquivo']["type"] == 'image/bmp'){
                                        
                                        preg_match("/.(gif|bmp|png|jpg|jpeg){1}$/i", $_FILES['arquivo']["name"], $ext);
                                        $nome_imagem_escola     = md5(uniqid(time())) . "." . $ext[1];
                                        $dados['logo_escola']   = "imagens/arquivo_usuarios/logo_escola/" . $nome_imagem_escola;
                                        move_uploaded_file($_FILES['arquivo']["tmp_name"], $dados['logo_escola']);

                                }else{
                                        /*      localizacao padrao  da imagem     */
                                        $dados['logo_escola'] = "imagens/arquivo_usuarios/logo_escola/logo_padrao.png";
                                }

                                if($_FILES['arquivo2']["type"] == 'image/png' || $_FILES['arquivo2']["type"] == 'image/jpeg' || $_FILES['arquivo2']["type"] == 'image/gif' || $_FILES['arquivo2']["type"] == 'image/bmp'){
                                        
                                        preg_match("/.(gif|bmp|png|jpg|jpeg){1}$/i", $_FILES['arquivo2']["name"], $ext);
                                        $nome_imagem_escola     = md5(uniqid(time())) . "." . $ext[1];
                                        $dados['plano_fundo']   = "imagens/arquivo_usuarios/banner_portal/" . $nome_imagem_escola;
                                        move_uploaded_file($_FILES['arquivo2']["tmp_name"], $dados['plano_fundo']);

                                }else{
                                        /*      localizacao padrao  da imagem     */
                                        $dados['plano_fundo'] = "imagens/arquivo_usuarios/banner_portal/fundo_padrao.png";
                                }

                                if($objPortal->inserir($dados)){
                                        $url    = $objConfig->getDados('pathView');
                                        $url   .= "dashboardAdm.php";
                                }else{
                                        $_SESSION['error'] = "Erro ao cadastrar portal... Desculpe-nos pelo transtorno, tente novamente mais tarde.";
                                        header("location: ".$objConfig->getDados('pathError'));
                                        die;
                                }

                        }

                }else{
                        $url    = $objConfig->getDados('pathView');
                        $url   .= "dashboardAdm.php";
                }

                include $url;

        }

        public function __call($method,$values){
        	$objConfig = new Config();
                $_SESSION['error'] = "url não encontrada.";
        	header("location: ".$objConfig->getDados('pathError'));
			die;
        }

}

?>
