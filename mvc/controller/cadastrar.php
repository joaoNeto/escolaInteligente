<?php

include "validarDados.php";

class Cadastrar{

        public function escola(){

                $objConfig    = new Config();
		
                $urlEscola       = $objConfig->getDados('pathModel');
                $urlEscola      .= "escola.php";
                
                $urlAdm          = $objConfig->getDados('pathModel');
                $urlAdm         .= "adm.php";
                
                include $urlEscola;
                include $urlAdm;

                $objEscola              = new Escola();
                $objAdm                 = new Adm();
                $objValidarDados        = new ValidarDados();
                $objPhpassword          = new Phpassword();

                /*      VALIDAR DADOS      */

                date_default_timezone_set('America/Recife');

                $dados = array();             
                $dados['nome_completo']         = (empty($_POST['nome_completo']))              ? $_POST['nome_completo2']      : $_POST['nome_completo'];
                $dados['email']                 = (empty($_POST['email']))                      ? $_POST['email2']              : $_POST['email'];
                $dados['ser_chamado']           = (empty($_POST['ser_chamado']))                ? $_POST['ser_chamado2']        : $_POST['ser_chamado'];  
                $dados['tel_movel']             = $_POST['tel_movel'];
                $dados['tel_fixo']              = $_POST['tel_fixo'];
                $dados['nome_instituicao']      = $_POST['nome_instituicao'];
                $dados['cnpj']                  = $_POST['cnpj'];
                $dados['inscr_estadual']        = $_POST['inscr_estadual'];
                $dados['tel_fixo_escola']       = $_POST['tel_fixo_escola'];
                $dados['cep']                   = $_POST['cep'];
                $dados['cidade']                = $_POST['cidade'];
                $dados['bairro']                = $_POST['bairro'];
                $dados['rua']                   = $_POST['rua'];
                $dados['numero']                = $_POST['numero'];
                $dados['complemento']           = $_POST['complemento'];
                $dados['data']                  = date("d/m/y - h:m:s");

                if(!($objValidarDados->validarEmail($dados['email']))){
                        $_SESSION['error'] = "O email é invalido.";
                        header("location: ".$objConfig->getDados('pathError'));
                        die();
                }

                if($objValidarDados->validarSenha($_POST['senha'],$_POST['senhaRep'])){

                        $dados['senha'] = $objPhpassword->passwordCrypt($_POST['senha']);

                }else{
                        $_SESSION['error'] = "As senhas digitadas no fomulário não coincidem.";
                        header("location: ".$objConfig->getDados('pathError'));
                        die();
                }

                if($_POST['CadastrarEscola'] == "Cadastrar escola no Plano Gold"){
                        $dados['plano'] = "4";
                }else if($_POST['CadastrarEscola'] == "Cadastrar escola no Plano Senior"){
                        $dados['plano'] = "2";
                }else{
                        $dados['plano'] = "1";
                }

                /*      CADASTRAR ESCOLA E ADM      */
               
                if(!$objEscola->inserir($dados)){

                        $_SESSION['error'] = "Não foi possivel cadastrar a escola pois o nome da escola, cnpj ou inscrição estadual ja está cadastrado no sistema.<br> Se Voce deseja reinvidicar a escola, favor entrar em contato <b>clicando aqui</b>."; 
                        header("location: ".$objConfig->getDados('pathError'));
                        $error['inserir_escola'] = '';
                        die();

                }

                $dados['id_escola']     = $objEscola->buscarId($dados['nome_instituicao'])->id;
                
                if(!$objAdm->inserir($dados)){
                        
                        $buscarEscola = (array) $objEscola->buscar($dados['nome_instituicao'],$dados['cnpj'],$dados['inscr_estadual']);    
                        $objEscola->deletarUm($buscarEscola['id']);
                      
                        $_SESSION['error'] = "Não foi possivel cadastrar o administrador pois o email do usuário ja está cadastrado no sistema.<br> Se Voce deseja reinvidicar o email, favor entrar em contato <b>clicando aqui</b>."; 
                        header("location: ".$objConfig->getDados('pathError'));
                        die();

                }

                $_SESSION['id_adm']     = $objAdm->buscarId($dados['email'])->id;
                $_SESSION['id_escola']  = $objEscola->buscarId($dados['nome_instituicao'])->id;

                $url     = $objConfig->getDados('pathBase');
                $url    .= "index.php/dashboard/adm";
                header("location: ".$url);

        }

        public function __call($method,$values){
        	$objConfig = new Config();
        	header("location: ".$objConfig->getDados('pathError'));
			die;
        }

}

?>
