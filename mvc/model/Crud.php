<?php

require_once 'Condb.php';

abstract class Crud extends Condb{

	protected $table;
	
	
	public function buscarUm($id){
		
		$sql  = "SELECT * FROM $this->table WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':id', $id, PDO::PARAM_INT);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}

	public function buscarTodos(){

		$sql  = "SELECT * FROM $this->table ORDER BY id DESC LIMIT 0,10";
		$stmt = Condb::prepare($sql);
		$stmt-> execute();
		
		return $stmt->fetchAll();
	
	}

	public function deletarUm($id){

		$sql  = "DELETE FROM $this->table WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':id', $id, PDO::PARAM_INT);
		
		return $stmt-> execute();
	
	}

}


?>