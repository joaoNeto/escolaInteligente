<?php

require_once 'Crud.php';

class Portal extends Crud{

	
	protected $table = 'portal';

	public function inserir($dados){

		/* SE O PORTAL EXISTIR, LOGO VOCE DA UM UPDATE */
		
		$sql  = "INSERT INTO $this->table (banner_fundo,logo_escola,cor_primaria,cor_secundaria,posicao_login) VALUES (:banner_fundo,:logo_escola,:cor_primaria,:cor_secundaria,:posicao_login)";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':banner_fundo', 		$dados['plano_fundo']);
		$stmt-> bindParam(':logo_escola', 		$dados['logo_escola']);
		$stmt-> bindParam(':cor_primaria', 		$dados['cor_primaria']);
		$stmt-> bindParam(':cor_secundaria', 	$dados['cor_secundaria']);
		$stmt-> bindParam(':posicao_login', 	$dados['posicao_login']);

		return $stmt->execute();
		
	}
	
	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}
	
	public function buscar($nome_escola,$cnpj,$inscr_estadual){
		
		$sql  = "SELECT * FROM $this->table WHERE nome_escola=:nome_escola OR inscr_estadual=:inscr_estadual OR cnpj=:cnpj";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':inscr_estadual', $inscr_estadual);
		$stmt-> bindParam(':nome_escola', $nome_escola);
		$stmt-> bindParam(':cnpj', $cnpj);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}
	
	public function buscarId($nome_escola){

		$sql  = "SELECT id FROM $this->table WHERE nome_escola=:nome_escola";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome_escola', $nome_escola);
		$stmt-> execute();
		
		return $stmt->fetch();
	}


	public function buscarEscolaPeloId($id){

		$sql  = "SELECT * FROM $this->table WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':id', $id);
		$stmt-> execute();
		
		return $stmt->fetch();
	}


}

?>