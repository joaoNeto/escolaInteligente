<?php

require_once 'Crud.php';

class Adm extends Crud{

	
	protected $table = 'adm';

	public function inserir($dados){

		$buscarAdm = Adm::buscar($dados['email']);

		if(!empty($buscarAdm))
			return false;

		$sql  = "INSERT INTO $this->table (id_escola,nome_completo,senha,ser_chamado,email,tel_movel,tel_fixo,plano,data_criacao) VALUES (:id_escola,:nome_completo,:senha,:ser_chamado,:email,:tel_movel,:tel_fixo,:plano,:data_criacao)";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':id_escola', 		$dados['id_escola']);
		$stmt-> bindParam(':nome_completo', 	$dados['nome_completo']);
		$stmt-> bindParam(':senha', 			$dados['senha']);
		$stmt-> bindParam(':ser_chamado', 		$dados['ser_chamado']);
		$stmt-> bindParam(':email', 			$dados['email']);
		$stmt-> bindParam(':tel_movel', 		$dados['tel_movel']);
		$stmt-> bindParam(':tel_fixo', 			$dados['tel_fixo']);
		$stmt-> bindParam(':plano', 			$dados['plano']);
		$stmt-> bindParam(':data_criacao', 		$dados['data']);

		return $stmt->execute();
		
	}
	
	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}
	
	public function buscar($email){
		
		$sql  = "SELECT * FROM $this->table WHERE email=:email";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':email', $email);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}

	public function buscarId($email){

		$sql  = "SELECT id FROM $this->table WHERE email=:email";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':email', $email);
		$stmt-> execute();
		
		return $stmt->fetch();
	}	
	
}

?>