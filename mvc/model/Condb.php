<?php

class Condb extends Config{

        private static $instance;
        
        public static function getInstance(){

                $objConfig = new Config();
                       
                if(!isset(self::$instance)){
                        
                        try{

                                self::$instance = new PDO("{$objConfig->getDados('dbtype')}:host={$objConfig->getDados('host')};dbname={$objConfig->getDados('db')}","{$objConfig->getDados('user')}","{$objConfig->getDados('password')}");
                                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                                self::$instance->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
                                self::$instance->exec("SET CHARACTER SET utf8");

                        }catch(PDOException $e){

                                echo $e->getMessage();

                        }
                        
                }
                
                return self::$instance;
        }
        
        public static function prepare($sql){
                
                return self::getInstance()->prepare($sql);
                
        }
        
}

?>