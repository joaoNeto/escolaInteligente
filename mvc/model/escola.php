<?php

require_once 'Crud.php';

class Escola extends Crud{

	
	protected $table = 'escola';

	public function inserir($dados){

		$buscarEscola = Escola::buscar($dados['nome_instituicao'],$dados['cnpj'],$dados['inscr_estadual']);

		if(!empty($buscarEscola))
			return false;

		$sql  = "INSERT INTO $this->table (nome_escola,tel_escola,cnpj,inscr_estadual,cep,cidade,bairro,rua,numero,complemento) VALUES (:nome_escola,:tel_escola,:cnpj,:inscr_estadual,:cep,:cidade,:bairro,:rua,:numero,:complemento)";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome_escola', 	$dados['nome_instituicao']);
		$stmt-> bindParam(':tel_escola', 	$dados['tel_fixo_escola']);
		$stmt-> bindParam(':inscr_estadual',$dados['inscr_estadual']);
		$stmt-> bindParam(':cnpj', 			$dados['cnpj']);
		$stmt-> bindParam(':cep', 			$dados['cep']);
		$stmt-> bindParam(':cidade', 		$dados['cidade']);
		$stmt-> bindParam(':bairro', 		$dados['bairro']);
		$stmt-> bindParam(':rua', 			$dados['rua']);
		$stmt-> bindParam(':numero', 		$dados['numero']);
		$stmt-> bindParam(':complemento', 	$dados['complemento']);

		return $stmt->execute();
		
	}
	
	public function atualizar($id){
		
		$sql  = "UPDATE $this->table SET nome=:nome,email=:email WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome', $this->nome);
		$stmt-> bindParam(':email', $this->email);
		$stmt-> bindParam(':id', $id);
		
		return $stmt->execute();

	}
	
	public function buscar($nome_escola,$cnpj,$inscr_estadual){
		
		$sql  = "SELECT * FROM $this->table WHERE nome_escola=:nome_escola OR inscr_estadual=:inscr_estadual OR cnpj=:cnpj";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':inscr_estadual', $inscr_estadual);
		$stmt-> bindParam(':nome_escola', $nome_escola);
		$stmt-> bindParam(':cnpj', $cnpj);
		$stmt-> execute();
		
		return $stmt->fetch();
		
	}
	
	public function buscarId($nome_escola){

		$sql  = "SELECT id FROM $this->table WHERE nome_escola=:nome_escola";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':nome_escola', $nome_escola);
		$stmt-> execute();
		
		return $stmt->fetch();
	}


	public function buscarEscolaPeloId($id){

		$sql  = "SELECT * FROM $this->table WHERE id=:id";
		$stmt = Condb::prepare($sql);
		$stmt-> bindParam(':id', $id);
		$stmt-> execute();
		
		return $stmt->fetch();
	}


}

?>