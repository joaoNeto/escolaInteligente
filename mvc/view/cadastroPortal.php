<?php 
	$objConfig = new Config();
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <script data-require="angular.js@1.4.8" data-semver="1.4.8" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

  <link rel="stylesheet" href="<?php echo $objConfig->getDados('pathBase').$objConfig->getDados('pathView') ?>/css/main.css">
  <title>Cadastre seu portal</title>
</head>

<body ng-app="myApp" ng-controller="myCtrl">

<div class="classe-bem-vindo-portal"> 
	<center>
		<font id="pre-titulo">
			{{myHeader}}
		</font>
	</center>
</div>

<div class="main-form-portal"  v-if="image">

	<div class="form-edicao-portal" onmouseover="mostrarMenuForm()">
		
		<div class="topo">
			<table>
				<tr><td><font id="titulo"> Edite seu portal </font></td><td><font id="close" onclick="esconderMenuForm()"> x </font></td></tr>
				<tr><td><a href="<?php echo $objConfig->getDados('pathBase') ?>"><font id="link">Ir para o início</font></a></td><td></td></tr>
			</table>	
		</div>

		<div class="url">
			<font id="titulo">url do portal</font>

			<div id="url_portal">
				<?php echo $objConfig->getDados('pathBase'); ?>index.php/portal/<br>nomeDaEscola
			</div>

		</div>

		<div class="conteudo-form-portal">
			
			<form method="POST" enctype="multipart/form-data" action="#">

				<div class="topico-input">
					<font 	id="titulo-input">Insira o logotipo:</font>
					<input 	type="file" 	name="arquivo" 	id="file" class="inputfile"  @change="onFileChange" />
					<label 	for="file">escolher arquivo</label>
				</div>

				<div class="topico-input">
					<font 	id="titulo-input">Insira um plano de fundo:</font>
					<input 	type="file" 	name="arquivo2" 	id="file_fundo" class="inputfile_fundo" @change="onFileChange2" />
					<label 	for="file_fundo">escolher arquivo</label>
				</div>

				<div class="topico-input">
					<font ng-init="cor_primaria='#53a51c' "></font>
					<font 	id="titulo-input">Cor primária:</font>
					<input 	type="color" 	name="cor_primaria" 	 	v-model="cor_primaria" />
				</div>

				<div class="topico-input">
					<font 	ng-init="cor_secundaria='#196670' "></font>
					<font 	id="titulo-input">Cor secundária:</font>
					<input 	type="color" 	name="cor_secundaria"	 	v-model="cor_secundaria" />
				</div>
				
				<div class="topico-input">
					<font 	id="titulo-input">Posição do formulário de login:</font>

					<select 				name="posicao_login" 		v-model="posicao_login">
						<option value="none">Centro</option>
						<option value="left">Esquerdo</option>
						<option value="right">Direito</option>
					</select>

				</div>
				
				<div class="topico-input">
					<input type="submit" value="Salvar portal" id="botao-submit" >
				</div>

			</form>

		</div>

		<div class="rodape-form-portal">
			<center>
				Veja outros modelos
			</center>
		</div>

	</div>

	<div class="portal-frame" v-bind:style="{ backgroundImage: 'url(' + image2 + ')' }">
		
		<div class="topo-portal">
	    	<img :src="image" id="logo-tipo"> 
		</div>

		<div class="conteudo-portal">
			<center>			
				<div class="div-form-portal" v-bind:style="{ float:''+ posicao_login +'' }">
					<input type="text" 		name="login" 	id="input-login" 	placeholder="Digite seu email ou matricula" />
					<input type="password" 	name="password" id="input-password" placeholder="Digite sua senha" />
					<input type="button" 	name="Entrar"	id="input-submit"  	value="Entrar" v-bind:style="{backgroundColor:'' + cor_primaria +'' }" />
					<font id="input-lost-password" 	v-bind:style="{color:'' + cor_secundaria +'' }" >esqueceu sua senha?</font>
					<!-- 	style="background-color:{{cor_primaria}}"  -->
				</div>
			</center>
		</div>

		<div class="rodape-portal">
			
		</div>

	</div>

</div>

<script>

	var app = angular.module('myApp', []);
	app.controller('myCtrl', function($scope, $timeout) {
	  
	  $scope.myHeader = "Olá!";      	

	  $timeout(function () {
	  	$scope.myHeader = "Bem Vindo a escola inteligente!"; 
	  	
	  	$timeout(function () {  
		  	document.getElementsByClassName('classe-bem-vindo-portal')[0].style = "display:none !important";	
		  	document.getElementsByClassName('main-form-portal')[0].style = "display:block !important";	
	  	}, 2500);

	  }, 2000);



	});

	function esconderMenuForm(){
		document.getElementsByClassName('form-edicao-portal')[0].style = "margin-left:-24%";	
		document.getElementById('close').style = "display:none";	
	}

	function mostrarMenuForm(){
		document.getElementsByClassName('form-edicao-portal')[0].style = "margin-left:0%";	
		document.getElementById('close').style = "display:block";			
	}

</script>


<!-- <script src='https://rawgit.com/vuejs/vue/master/dist/vue.js'></script> -->

<script src="https://unpkg.com/vue"></script>

<script type="text/javascript">

	'use strict';

	new Vue({

	  el: '.main-form-portal',
	  data: {
	    image: '../../imagens/logotipo-escola-inteligente-largo.png',
	    image2: '../../imagens/fundo3.jpg',
	    cor_primaria:'#53a51c',
		cor_secundaria:'#196670',
		posicao_login:'none'   
	  },
	  methods: {

	    onFileChange: function onFileChange(e) {
	      var files = e.target.files || e.dataTransfer.files;
	      if (!files.length) return;
	        this.createImage(files[0]);
	    },

	    createImage: function createImage(file) {
	      var image = new Image();
	      var reader = new FileReader();
	      var vm = this;

	      reader.onload = function (e) {
	        vm.image = e.target.result;
	      };
	      reader.readAsDataURL(file);
	    },

	    onFileChange2: function onFileChange2(e) {
	      var files = e.target.files || e.dataTransfer.files;
	      if (!files.length) return;
	        this.createImage2(files[0]);
	    },

	    createImage2: function createImage2(file) {
	      var image2 = new Image();
	      var reader = new FileReader();
	      var vm = this;

	      reader.onload = function (e) {
	        vm.image2 = e.target.result;
	      };
	      reader.readAsDataURL(file);
	    },
	    
	  }

	});	

</script>


</body>
</html>