<?php 
  $objConfig = new Config();
?>
<!DOCTYPE html>
<html>
<head>
        <title>Bem Vindo a escola inteligente</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="mvc/view/css/main.css" />
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel="stylesheet" href="mvc/view/css/form-index.css">
        <link rel="stylesheet" href="mvc/view/css/icones.css">
        <link rel="stylesheet" href="mvc/view/css/planos.css">        
</head>
<body>


        <div class="cabecalho">
                <div class="topo">
                        <img src="imagens/logotipo-escola-inteligente-largo.png" id="logotipo">
                </div>
                <div class="banner">
                        <img alt="Discover Drive animation" src="https://gallery.mailchimp.com/8234ed62d6a7fa87721ff1d8a/images/Discover_Google_Drive_jw.gif" id="image-banner">                    
                </div>
                <div class="area-login">

                          <div class="form">
                              
                              <ul class="tab-group">
                                <li class="tab active"><a href="#signup">Entre</a></li>
                                <li class="tab"><a href="#login">Cadastre-se</a></li>
                              </ul>
                              
                              <div class="tab-content">
                                <div id="signup">   
                                  <h1>Faça seu login</h1>
                                  
                                  <form action="/" method="post">
                                  

                                  <div class="field-wrap">
                                    <label>
                                      Digite seu Email<span class="req">*</span>
                                    </label>
                                    <input type="email"required autocomplete="off"/>
                                  </div>
                                  
                                  <div class="field-wrap">
                                    <label>
                                      Digite sua Senha<span class="req">*</span>
                                    </label>
                                    <input type="password"required autocomplete="off"/>
                                  </div>

                                  <p class="forgot"><a href="#">Perdeu a senha?</a></p>

                                  <button type="submit" class="button button-block"/>Login</button>
                                  
                                  </form>

                                </div>
                                
                                <div id="login">  
                                <!-- http://localhost/escolainteligente/index.php/cadastro/home/ -> mandar pra ki -->

                                  <h1>Cadastre sua escola</h1>
                                  
                                  <form action="<?php echo $objConfig->getDados('pathBase'); ?>index.php/formulario/escola" method="POST">
                                  
                                    <div class="field-wrap">
                                    <label>
                                      Digite seu nome completo<span class="req">*</span>
                                    </label>
                                    <input type="text" autocomplete="off"  name="nome_completo" maxlength="250" />
                                  </div>
                                  
                                  <div class="field-wrap">
                                    <label>
                                      Digite seu email<span class="req">*</span>
                                    </label>
                                    <input type="email" autocomplete="off" name="email" maxlength="250" />
                                  </div>

                                  <div class="field-wrap">
                                    <label>
                                      como gostaria de ser chamado<span class="req">*</span>
                                    </label>
                                    <input type="text" autocomplete="off"  name="ser_chamado"  maxlength="20" />
                                  </div>

                                  <button class="button button-block"/>Continuar cadastro</button>
                                  
                                  </form>

                                </div>
                                
                              </div><!-- tab-content -->
                              
                        </div> <!-- /form -->
                                
                </div>
                
                <div class="corpo">

                  <font id="titulo-corpo">O que é a escola inteligente?</font>

                  <font id="preconteudo-corpo">
                    Escola inteligente é um site afim de oferecer suporte a escolas de ensino medio e fundamental com um portal educacional totalmente editavel, esse portal uma vez criado ira fazer com que todas as tarefas de sua escola seja automatizado isto ira trazer para sua escola varias vantagens como:
                  </font>

                  <center>
                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-browser"></span>
                      <font id="texto-vantagem">
                        O recurso portal educacional é a chave de todo o sistema escola inteligente, ao criar o portal educacional uma url é gerada que sera utilizada por todos os seus funcionarios, professores e alunos para entrar em suas respectivas contas, ao escolher um determinado portal voce poderá edita-lo para ficar mais parecido com a cara da sua escola. Com a função portal educacional voce gerencia 100% a sua escola.
                      </font>
                    </div>

                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-suitcase"></span>
                      <font id="texto-vantagem">
                        Diminua o esforço tanto para voce quanto para sua escola, com a função boleto bancario voce pode tirar a preocupação dos pais de seus alunos, agora o pagamento das mensalidades será via boleto bancario, despreocupe a vida de seus funcionarios e de seus clientes com mais esta vantagem.
                      </font>
                    </div>


                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-paper"></span>
                      <font id="texto-vantagem">
                        Com o recurso atividade online professor pode realizar atividades dinamicas para serem respondidas por alunos e para dinamizar ainda mais qualquer aluno pode tirar duvidas com o recurso tirar duvidas onde o mesmo poderá mandar mensagens e tirar duvidas com professor,cordenador ou até diretor da instituição.
                      </font>
                    </div>


                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-screen"></span>
                      <font id="texto-vantagem">
                        Voce professor ou aluno pode realizar avaliações online que podem ser disponibilizadas tanto para a escola, salas de aulas ou até algum aluno individual, com este recurso o professor podera avaliar melhor seus alunos recebendo estatisticas em tempo real do desempenho de todos os eles, sem falar que alunos também podem lançar avaliações para que outros alunos também respondam e assim aumentar a iteratividade entre os mesmos.
                      </font>
                    </div>

                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-message"></span>
                      <font id="texto-vantagem">
                        Facilite a comunicação entre todas as partes da sua escola com a função de troca de mensagens inteligente. melhore a comunicação
                        dos professores com os alunos e de todos os seus funcionários com os seus alunos, voce pode a qualquer momento manipular a lojistica de troca de mensagens da sua escola.
                      </font>
                    </div>


                    <div id="topico-vantagem">
                      
                      <span class="svg-icon flat-shadow" id="shadow-graph"></span>
                      <font id="texto-vantagem">
                        A função gerenciar orçamento é ideal para voce que deseja uma organização melhor os orçamentos da sua escola, voce poderá organizar todas as suas contas a pagar e receber além de ficar por dentro de lucros mensais e contas mensais.
                      </font>
                    </div>

                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-home"></span>
                      <font id="texto-vantagem">
                        Voce também pode automatizar a sua biblioteca, com o recurso biblioteca online você tera total controle do estoque de seus livros, além de obter todo o feedback do que ocorre com sua biblioteca em tempo real.
                      </font>
                    </div>


                    <div id="topico-vantagem">
                      <span class="svg-icon flat-shadow" id="shadow-settings"></span>
                      <font id="texto-vantagem">
                        Voce notou que o sistema da escola inteligente não satisfaz todas as suas necessidades? não tem problema, procure imediatamente alguma extensão que complementa a sua escola inteligente, se mesmo assim voce não encontrou alguma extensão que solucione seu problema peça ja uma reunião com o suporte para desenvolver para voce.
                      </font>
                    </div>

               
                  </center>


                </div> 

            <div class="planos">
              
              <font id="titulo-planos">Planos e preços</font>

              <div class="plans">
              <div class="plan-box">
                <div class="plan-options">
                  <p class="plan-name">
                    Básico
                  </p>
                  <div class="plan-price">
                    <strong>R$ 30</strong>,00 /mes
                  </div>
                   <ul class="plan-details">
                      <li>10 Salas de aula</li>
                      <li>100 Alunos</li>
                      <li>10 Funcionários e professores</li>
                      <li>Função Portal educacional</li>
                      <li>Função Troca de mensagens</li>
                      <li>Função de controle de gestão e orçamento</li>
                  </ul>
                  
                  <a class="button" href="<?php echo $objConfig->getDados('pathBase'); ?>index.php/formulario/escola">Escolher</a>
                </div>
              </div>
              
              <div class="plan-box">
                <div class="plan-options">
                  <p class="plan-name">
                    Senior
                  </p>
                  <div class="plan-price">
                    <strong>R$50</strong>,00/mes
                  </div>
                   <ul class="plan-details">
                      <li>100 Salas de aula</li>
                      <li>1000 Alunos</li>
                      <li>100 Funcionários e professores</li>
                      <li>Função Portal educacional</li>
                      <li>Função Troca de mensagens</li>
                      <li>Função de controle de gestão e orçamento</li>
                      <li><b>Função Bibliotéca online</b></li>
                      <li><b>Função Avaliação online</b></li>
                      <li><b>Função Ata online</b></li>
                  </ul>
                  
                  
                  
                  <!-- div class="best-choice">
                      Mais pedido pelas escolas...
                  </div> -->
                  
                  <a class="button" href="<?php echo $objConfig->getDados('pathBase'); ?>index.php/formulario/escola">Escolher</a>
                </div>
              </div>
              
              <div class="plan-box">
                <div class="plan-options">
                  <p class="plan-name">
                    Gold
                  </p>
                  <div class="plan-price">
                    <strong>R$100</strong>,00/mes
                  </div>
                    <ul class="plan-details">
                      <li>∞ Salas de aula</li>
                      <li>∞ Alunos</li>
                      <li>∞ Funcionários e professores</li>
                      <li>Função Portal educacional</li>
                      <li>Função Troca de mensagens</li>
                      <li>Função de controle de gestão e orçamento</li>
                      <li>Função Bibliotéca online</li>
                      <li>Função Avaliação online</li>
                      <li>Função Ata online</li>
                      <li>Função Mensalidades online</li>
                      <li>3 extensões grátis</li>
                    </ul>
                
                  <a class="button" href="<?php echo $objConfig->getDados('pathBase'); ?>index.php/formulario/escola">Escolher</a>
                </div>
              </div>
            </div>


            </div>

            <div class="rodape">
              Desenvolvido pela: 
              Avant Comunicação Digital
            </div>

        </div> 

 

</body>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="mvc/view/js/form-index.js"></script>
  <script src="mvc/view/js/icones.js"></script>
</html>