'use strict';

new Vue({

  el: '.maisumteste',
  data: {
    image: 'MgaV9.png',
    image2: 'logoEscolaInteligente(icone).png'
  },
  methods: {

    onFileChange: function onFileChange(e) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
        this.createImage(files[0]);
    },

    createImage: function createImage(file) {
      var image = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = function (e) {
        vm.image = e.target.result;
      };
      reader.readAsDataURL(file);
    },

    /*  NOVA IMAGEM   */

    onFileChange2: function onFileChange2(e) {
      var files = e.target.files || e.dataTransfer.files;
      if (!files.length) return;
        this.createImage2(files[0]);
    },

    createImage2: function createImage2(file) {
      var image2 = new Image();
      var reader = new FileReader();
      var vm = this;

      reader.onload = function (e) {
        vm.image2 = e.target.result;
      };
      reader.readAsDataURL(file);
    },



  }

});