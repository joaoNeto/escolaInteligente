<?php 

$nome_completo    = "nome completo";
$email            = "email";
$ser_chamado      = "como gostaria de ser chamado";

if(!empty($_POST['nome_completo'])){
  $nome_completo  = $_POST['nome_completo'];
}
if(!empty($_POST['email'])){
  $email          = $_POST['email'];
}
if(!empty($_POST['ser_chamado'])){
  $ser_chamado    = $_POST['ser_chamado'];
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Cadastre sua escola</title>
  <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">  
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Raleway:400,500,800">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
  <link rel="stylesheet" href="<?php echo $objConfig->getDados('pathBase'); ?>mvc/view/css/form-cadastro-escola.css">
  <link rel="stylesheet" href="<?php echo $objConfig->getDados('pathBase'); ?>mvc/view/css/form-index.css">
</head>

<body>

<div class="topo-cadastre-se">
Cadastre-se
</div>

<center>
<div id="alertaSenha">
  <font style="float:right;cursor:pointer" onclick="esconderAlert();">x</font>
  <br>
  As senhas não coincidem.
</div>
</center>

<div id="wrapper">
  <div id="left-side">
    <ul>

      <li class="topico-cadastro-escola">
        Sobre voce
      </li>

      <li class="topico-cadastro-escola">
        Sobre sua escola
      </li>

      <li class="topico-cadastro-escola">
        Sobre a localização
      </li>

      <li class="topico-cadastro-escola">
        Seu plano
      </li>

    </ul>
  </div>

  <div id="border">
    <div id="line" class="one"></div>
  </div>

  <div id="right-side">
    <div id="first" class="active">
      <form method="POST" action="<?php echo $objConfig->getDados('pathBase'); ?>index.php/cadastrar/escola" id="formularioCadastro">
        
        <center><h1>Sobre voce</h1></center>

        <div class="form">
          
          <input type='hidden' name='nome_completo2'   value='<?php echo $nome_completo; ?>'>
          <input type='hidden' name='email2'           value='<?php echo $email; ?>'>
          <input type='hidden' name='ser_chamado2'     value='<?php echo $ser_chamado; ?>'>

          <div class="field-wrap">
            <label><?php echo $nome_completo; ?></label>
            <input    type="text"       autocomplete="off"  id="nome_completo"    name="nome_completo"    maxlength="250" />
          </div>

          <div class="field-wrap">
            <label><?php echo $email; ?></label>
            <input    type="email"      autocomplete="off"  id="email"            name="email"            maxlength="250" />
          </div>

          <div class="field-wrap">
            <label>Tel. Movel</label>
            <input    type="text"       autocomplete="off"  id="tel_movel"        name="tel_movel"        onKeyPress="MascaraTelefone(tel_movel);" maxlength="14" />
          </div>

          <div class="field-wrap">
            <label>Tel. Fixo</label>
            <input    type="text"       autocomplete="off"  id="tel_fixo"         name="tel_fixo"         onKeyPress="MascaraTelefone(tel_fixo);" maxlength="14"   />
          </div>

          <div class="field-wrap">
            <label>Senha<span class="req">*</span></label>
            <input    type="password"   autocomplete="off"  id="senha"            name="senha"           maxlength="250" required />
          </div>

          <div class="field-wrap">
            <label>Repitir senha<span class="req">*</span></label>
            <input    type="password"   autocomplete="off"  id="senhaRep"         name="senhaRep"        maxlength="250" required />
          </div>
         
          <div class="field-wrap">
            <label><?php echo $ser_chamado; ?></label>
            <input    type="text"       autocomplete="off"  id="ser_chamado"      name="ser_chamado"      maxlength="20" />
          </div>

          <div class="button button-block sobre-voce pay" onclick="mostrarBotaoSuaEscola();"/>Prosseguir</div>

        </div>

    </div>
    <div id="second">
      <center><h1>Sobre sua escola</h1></center>

      <div class="form">
        
        <div class="field-wrap">
          <label>
           Nome da instituição<span class="req">*</span>
          </label>
          <input    type="text"       autocomplete="off"  id="nome_instituicao"       name="nome_instituicao"       maxlength="250"     required />
        </div>

        <div class="field-wrap">
          <label>
            CNPJ<span class="req">*</span>
          </label>
          <input type="text" autocomplete="off"  id="cnpj" name="cnpj" onKeyPress="MascaraCNPJ(cnpj);" maxlength="18" required />
         </div>

        <div class="field-wrap">
          <label>
            Inscr. Estadual<span class="req">*</span>
          </label>
          <input    type="text"       autocomplete="off"  id="inscr_estadual"  onKeyPress="MascaraInscrEstadual(inscr_estadual);"  name="inscr_estadual"         maxlength="15"  required />
        </div>

        <div class="field-wrap">
          <label>
            Tel. fixo
          </label>
            <input    type="text"     autocomplete="off"  id="tel_fixo_escola"        name="tel_fixo_escola"   onKeyPress="MascaraTelefone(tel_fixo_escola);" maxlength="14" />
        </div>

        <div class="button button-block choose active"  onclick="mostrarBotaoSobreVoce();" />Voltar</div>
        <div class="button button-block wrap"  onclick="mostrarBotaoSuaLocalizacao();" />Prosseguir</div>
      </div>


    </div>
    <div id="third">

      <center><h1 id="center-h1">Sobre a localização da sua escola</h1></center>

       <div class="form">
        
        <div class="field-wrap">
          <label>
           CEP<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off" name="cep" id="cep" onKeyPress="MascaraCep(cep);" maxlength="10" />
        </div>

        <div class="field-wrap">
          <label>
            Cidade<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off"  name="cidade" id="cidade" maxlength="250" />
        </div>

        <div class="field-wrap">
          <label>
            Bairro<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off"  name="bairro" id="bairro" maxlength="250" />
        </div>

        <div class="field-wrap">
          <label>
            Rua<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off"  name="rua" id="rua" maxlength="250" />
        </div>

        <div class="field-wrap">
          <label>
            Numero<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off"  name="numero" id="numero" maxlength="250" />
        </div>

        <div class="field-wrap">
          <label>
            Complemento<span class="req">*</span>
          </label>
          <input type="text" required autocomplete="off"  name="complemento" id="complemento" maxlength="250" />
        </div>

        <div class="button button-block choose pay"   onclick="mostrarBotaoSuaEscola();" />Voltar</div>
        <div class="button button-block ship"   onclick="mostrarBotaoSeuPlano();" />Prosseguir</div>


      </div>

    </div>
    <div id="fourth">

      <center><h1>Seu plano</h1></center>

      <div class="plano-detalhe">
          <div class="etapa-plano">

            <div id="titulo-etapa-plano">Básico R$ 30,00/mes</div>

            <div id="vantagens-etapa-plano">
              <br>10 Salas de aula 
              <br>100 Alunos
              <br>10 Funcionários e professores
              <br>Função Portal educacional
              <br>Função Troca de mensagens
              <br>Função de controle de gestão e orçamento
            </div>

            <input type="submit" name="CadastrarEscola" value="Cadastrar escola no Plano Básico" id="botao-etapa-plano">

          </div>
          <div class="etapa-plano">
 
            <div id="titulo-etapa-plano">Senior R$ 50,00/mes</div>
          
            <div id="vantagens-etapa-plano">
              <br>100 Salas de aula
              <br>1000 Alunos
              <br>100 Funcionários e professores
              <br>Função Portal educacional
              <br>Função Troca de mensagens
              <br>Função de controle de gestão e orçamento
              <br>Função Bibliotéca online
              <br>Função Avaliação online
              <br>Função Ata online
            </div>

            <input type="submit" name="CadastrarEscola" value="Cadastrar escola no Plano Senior" id="botao-etapa-plano">    
 
          </div>
          <div class="etapa-plano">

            <div id="titulo-etapa-plano">Gold R$ 100,00/mes</div>

            <div id="vantagens-etapa-plano">
              <br>∞ Salas de aula
              <br>∞ Alunos
              <br>∞ Funcionários e professores
              <br>Função Portal educacional
              <br>Função Troca de mensagens
              <br>Função de controle de gestão e orçamento
              <br>Função Bibliotéca online
              <br>Função Avaliação online
              <br>Função Ata online
              <br>Função Mensalidades online
              <br>3 extensões grátis
            </div>  

            <input type="submit" name="CadastrarEscola" value="Cadastrar escola no Plano Gold" id="botao-etapa-plano">
            
          </div>
      </div>

      <div class="rodape-plano">

        Ao selecionar algum plano, você concorda com nossos <b>Termos</b> e que leu nossa <b>Política de Dados</b>.

        <input type="button" name="voltar" value="voltar" class="wrap" id="botao-voltar"  onclick="mostrarBotaoSuaLocalizacao();"  >   
        
      </div>

    </div>
    </form>
  </div>
</div>

<div class="rodape-cadastro">
  <a href="<?php echo $objConfig->getDados('pathBase'); ?>" id="fonte-inicio">Voltar ao inicio</a>
  <img src="<?php echo $objConfig->getDados('pathBase'); ?>imagens/logotipo-escola-inteligente-largo.png" id="logotipo-escola-inteligente">
</div>

  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
  <script src="<?php echo $objConfig->getDados('pathBase'); ?>mvc/view/js/form-cadastro.js"></script>
  <script src="<?php echo $objConfig->getDados('pathBase'); ?>mvc/view/js/form-index.js"></script>  

<script type="text/javascript">
  
  var classe      = document.getElementsByClassName("button");
  var topico      = document.getElementsByClassName("topico-cadastro-escola");

   topico[0].style.color = "white";    
 
   mostrarBotaoSobreVoce();

  function mostrarBotaoSobreVoce(){

    for(var i=0; i<=3;i++)
      if(i == 0)
        topico[i].style.color   = "white";    
      else
        topico[i].style.color   = "#a0b3b0";    


    for(var i=0; i<=4;i++)
      if(i == 0)
        classe[i].style.margin  = "0px";    
      else
        classe[i].style.margin  = "-5000px";      

    document.getElementById("first").style = "display:block !important";
    document.getElementById("second").style = "display:none !important";
    document.getElementById("third").style = "display:none !important";
    document.getElementById("fourth").style = "display:none !important";


  }

  function mostrarBotaoSuaEscola(){

   NovaSenha = document.getElementById('senha').value;
   CNovaSenha = document.getElementById('senhaRep').value;
   if (NovaSenha != CNovaSenha)
      document.getElementById('alertaSenha').style = "display:block"; 

    for(var i=0; i<=3;i++)
      if(i == 1)
        topico[i].style.color   = "white";    
      else
        topico[i].style.color   = "#a0b3b0";

    for(var i=0; i<=4;i++)
      if(i == 1 || i == 2)
        classe[i].style.margin  = "0px";    
      else
        classe[i].style.margin  = "-5000px";

    document.getElementById("first").style = "display:none !important";
    document.getElementById("second").style = "display:block !important";
    document.getElementById("third").style = "display:none !important";
    document.getElementById("fourth").style = "display:none !important";


  }

  function mostrarBotaoSuaLocalizacao(){

    for(var i=0; i<=3;i++)
      if(i == 2)
        topico[i].style.color   = "white";    
      else
        topico[i].style.color   = "#a0b3b0";

    for(var i=0; i<=4;i++)
      if(i == 3 || i == 4)
        classe[i].style.margin  = "0px";    
      else
        classe[i].style.margin  = "-5000px";

    document.getElementById("first").style = "display:none !important";
    document.getElementById("second").style = "display:none !important";
    document.getElementById("third").style = "display:block !important";
    document.getElementById("fourth").style = "display:none !important";


  }

  function mostrarBotaoSeuPlano(){

    for(var i=0; i<=3;i++)
      if(i == 3)
        topico[i].style.color   = "white";    
      else
        topico[i].style.color   = "#a0b3b0";

    for(var i=0; i<=4;i++)
        classe[i].style.margin  = "-5000px";

    document.getElementById("first").style = "display:none !important";
    document.getElementById("second").style = "display:none !important";
    document.getElementById("third").style = "display:none !important";
    document.getElementById("fourth").style = "display:block !important";


  } 

  function esconderAlert(){
    document.getElementById("alertaSenha").style = "display:none";    
  }


// JavaScript Document
//adiciona mascara de cnpj
function MascaraCNPJ(cnpj){
        if(mascaraInteiro(cnpj)==false){
                event.returnValue = false;
        }       
        return formataCampo(cnpj, '00.000.000/0000-00', event);
}

function MascaraInscrEstadual(inscr_estadual){
        if(mascaraInteiro(inscr_estadual)==false){
                event.returnValue = false;
        }       
        return formataCampo(inscr_estadual, '000.000.000.000', event);
}

//adiciona mascara de cep
function MascaraCep(cep){
                if(mascaraInteiro(cep)==false){
                event.returnValue = false;
        }       
        return formataCampo(cep, '00.000-000', event);
}

//adiciona mascara de data
function MascaraData(data){
        if(mascaraInteiro(data)==false){
                event.returnValue = false;
        }       
        return formataCampo(data, '00/00/0000', event);
}

//adiciona mascara ao telefone
function MascaraTelefone(tel){  
        if(mascaraInteiro(tel)==false){
                event.returnValue = false;
        }       
        return formataCampo(tel, '(00) 0000-0000', event);
}

//adiciona mascara ao CPF
function MascaraCPF(cpf){
        if(mascaraInteiro(cpf)==false){
                event.returnValue = false;
        }       
        return formataCampo(cpf, '000.000.000-00', event);
}

//valida numero inteiro com mascara
function mascaraInteiro(){
        if (event.keyCode < 48 || event.keyCode > 57){
                event.returnValue = false;
                return false;
        }
        return true;
}

//formata de forma generica os campos
function formataCampo(campo, Mascara, evento) { 
        var boleanoMascara; 

        var Digitato = evento.keyCode;
        exp = /\-|\.|\/|\(|\)| /g
        campoSoNumeros = campo.value.toString().replace( exp, "" ); 

        var posicaoCampo = 0;    
        var NovoValorCampo="";
        var TamanhoMascara = campoSoNumeros.length;; 

        if (Digitato != 8) { // backspace 
                for(i=0; i<= TamanhoMascara; i++) { 
                        boleanoMascara  = ((Mascara.charAt(i) == "-") || (Mascara.charAt(i) == ".")
                                                                || (Mascara.charAt(i) == "/")) 
                        boleanoMascara  = boleanoMascara || ((Mascara.charAt(i) == "(") 
                                                                || (Mascara.charAt(i) == ")") || (Mascara.charAt(i) == " ")) 
                        if (boleanoMascara) { 
                                NovoValorCampo += Mascara.charAt(i); 
                                  TamanhoMascara++;
                        }else { 
                                NovoValorCampo += campoSoNumeros.charAt(posicaoCampo); 
                                posicaoCampo++; 
                          }              
                  }      
                campo.value = NovoValorCampo;
                  return true; 
        }else { 
                return true; 
        }
}

 

</script>

</body>
</html>
