<?php

	include "config.php";
	$objConfig = new Config();

	session_start();

?>

<!DOCTYPE html>
<html>
<head>
	<title>Error</title>
	<link rel="stylesheet" href="<?php echo $objConfig->getDados('pathBase'); ?>mvc/view/css/main.css">
    <meta charset="UTF-8">	
</head>
<body>

	<div class="pagError">
		<center>
			<div class="mensagem-error">

				<div id="conteudo-error">
					<font id="fonte-error-01">Erro... :(</font>
					<br>
					<font id="fonte-error-02">Algo não desejado ocorreu no sistema.</font>
					<br>
					<font id="fonte-error-03"><font style="font-family:'Futura Md BT';">Mensagem.:</font> <?php print $_SESSION['error']; ?></font>
					<br>
					<a href="<?php echo $objConfig->getDados('pathBase'); ?>" id="link-error">Voltar ao site</a>
				</div>

				<div id="imagem"></div>

			</div>
		</center>
	</div>

</body>
</html>