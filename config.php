<?php


class Config{

	private $dados = [
    
    // BANCO DE DADOS
    "db" 				=> "bd_escola_inteligente",
    "host" 				=> "localhost",
    "user" 				=> "root",
    "password" 			=> "",
    "dbtype" 			=> "mysql",
    "dbprefix" 			=> "ei_",

    // SEGURANÇA
    "salt" 				=> "#sCx23214C4G98DJcjrk123coe1S2CK3dk39.",
    "prefix" 			=> "&PS",

    // DIRETORIOS
    "pathBase" 			=> "http://localhost/portal/",
    "pathController"	=> "mvc/controller/",
    "pathModel" 		=> "mvc/model/",
    "pathView" 			=> "mvc/view/",
    "pathError"         => "http://localhost/portal/error.php",

    // EMAILS
    "mailfrom" 			=> "",
    "fromname" 			=> "",
    "sendmail" 			=> ""
	
	];

	public function getDados($key){
		return $this->dados[$key];
	}

	
}


?>