<?php
include "config.php";
include "bibliotecas/phpassword.php";

session_start();

$objConfig 	= new Config();
$objPass 	= new Phpassword();

if(empty($_SERVER['PATH_INFO'])){

	$classe 	= "index";
	$metodo 	= "home";

}else{

	$path_info 	= explode('/',$_SERVER['PATH_INFO']);
	$classe 	= $path_info[1];
	$metodo 	= $path_info[2];

}

$url  =	$objConfig->getDados('pathController');
$url .=	$classe.".php";

if(!include $url){
	$_SESSION['error'] = "A url não foi encontrada.";
	header("location: ".$objConfig->getDados('pathError'));
	die;
}

$obj = new $classe();
$obj->$metodo();

?>